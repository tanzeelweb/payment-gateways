<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace app\PGs;
/**
 * Description of pginterface
 *
 * @author tanzeel
 */
interface pginterface {
    public function transaction();
    public function refund();
    public function status();
}
